FROM golang:1.13

WORKDIR /go/src/app
COPY . .

RUN go get -d -v ./...
RUN go install -v ./...
# RUN go build .

ENV PORT 8000
CMD app $PORT
# CMD ./app $PORT
EXPOSE $PORT/tcp